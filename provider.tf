provider "aws"
{
  #Create the profile first or prive the access_key and secret_key
  profile = "houston"
  region = "us-east-1"
}

# Below config is for multi-regionoks
# The alias name below MUST match the form given in the matching region.tf
# file (ex: alias = "us-west-1" matches providers = { aws = "aws.us-west-1" })

#provider "aws"
#{
#  alias = "us-east-2"
#  profile = "houston"
#  region = "us-east-2"
#}
