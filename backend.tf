terraform {
  backend "s3" {
    #provide the access_key and secret_key of the aws account to deploy these resourses
    access_key = ""
    secret_key = ""
    region     = "us-east-1"

    bucket = "terraform-aws-dev"
    key    = "testdeploy"

    dynamodb_table = "terraform-aws-dev"
  }
}
