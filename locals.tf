locals
{
deployment_name = "testdeploy"
production = false
orion_base_url = ""
zone_id = "" # Route53 zone for DNS
chargecode = "36472727" # Could be anything
consumer = "Tax QA app testing" 
techcontacts = "orion-alert@git-ventures.pagerduty.com" #pagerduty info
RABBITMQ_PASSWD = "quaiwahKoogha3"
min_ecs_task_percent = "0"
max_ecs_task_percent = "100"
upload_watcher_s3_bucket = "ey-upload-watcher"
docker_vm_size = "t2.large"
db_name = "houston"
rds_username = "root"
rds_password = ""
basic_auth_username = "testusername"
#provide the application password
basic_auth_password = ""
job_status_docker_image_url = ""
job_runner_docker_image_url = ""
api_gateway_docker_image_url = ""
ey_houston_env_docker_url = ""
job_status_docker_image_tag = "latest"
job_runner_docker_image_tag = "latest"
api_gateway_docker_image_tag = "latest"

# Storage Manager AWS ARN
buckets_access_principles_arn = ""
}
