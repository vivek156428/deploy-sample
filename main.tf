module "master" {
  source                 = ""                             # provide the source for the main code
  orion_base_url         = "${local.orion_base_url}"
  production             = "${local.production}"
  is_master              = "true"                                                               # is it master? if so write true, otherwise leave blank
  deployment_name        = "${local.deployment_name}"
  chargecode             = "${local.chargecode}"
  consumer               = "${local.consumer}"
  techcontacts           = "${local.techcontacts}"
  zone_id                = "${local.zone_id}"
  rabbit_host            = "${local.rabbit_host}"
  rabbit_username        = "smrabbit"
  rabbit_password        = "${local.RABBITMQ_PASSWD}"
  basic_auth_username    = "${local.basic_auth_username}"
  basic_auth_password    = "${local.basic_auth_password}"
  authorized_keys        = ""
  public_key             = "${file("~/.ssh/id_rsa.pub")}"                                       # this is for ECS and jumper machines
  db_name                = "${local.db_name}"
  rds_username           = "${local.rds_username}"
  rds_password           = "${local.rds_password}"
  data_bucket_sprintf    = "orion-${local.deployment_name}-data-%s"
  landing_bucket_sprintf = "orion-${local.deployment_name}-landing-%s"
  ecr_region             = "us-west-2"                                                          # ECR region for the docker images

  job_status_docker_image_url  = "${local.job_status_docker_image_url}"
  job_runner_docker_image_url  = "${local.job_runner_docker_image_url}"
  api_gateway_docker_image_url = "${local.api_gateway_docker_image_url}"
  ey_houston_env_docker_url    = "${local.ey_houston_env_docker_url}"

  job_status_docker_image_tag  = "${local.job_status_docker_image_tag}"
  job_runner_docker_image_tag  = "${local.job_runner_docker_image_tag}"
  api_gateway_docker_image_tag = "${local.api_gateway_docker_image_tag}"

  buckets_access_principles_arn = "${local.buckets_access_principles_arn}" # Who can access to your s3 bucket
  min_ecs_task_percent          = "${local.min_ecs_task_percent}"
  max_ecs_task_percent          = "${local.max_ecs_task_percent}"
  upload_watcher_s3_bucket      = "${local.upload_watcher_s3_bucket}"
  docker_vm_size                = "${local.docker_vm_size}"
}
